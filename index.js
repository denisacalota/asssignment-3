
const FIRST_NAME = "Denisa Andreea";
const LAST_NAME = "Calota";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name=name; 
        this.surname=surname; 
        this.salary=salary;
    }
    
   /* get name ()
    {
        return this._name;
    }

    get surname()
    {
        return this._surname;
    }

    get salary()
    {
        return this._salary;
    }

    set name (value)
    {
        this._name=value;
    }
    set surname(value)
    {
        this._surname= value;
    }
    set salary(value)
    {
        this._salary =value;
    }*/


    getDetails() {
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience)
    {
        super(name, surname,salary);
        if(experience == undefined)
        {
            this.experience='JUNIOR';
        }
        else
        {
            this.experience=experience;
        }
    }

    applyBonus()
    {
        if( this.experience ==='MIDDLE')
            return this.salary*1.15;
            else
                if( this.experience ==='JUNIOR')
                    return this.salary*1.1;
                else
                if( this.experience ==='SENIOR')
                    return this.salary*1.2;
                else
                    return this.salary*1.1;
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

